# Get info from IdCard

从身份证号获取信息

安装

```bash
npm i get-id-card-info
```

使用

```js
const getIdCardInfo = require("get-id-card-info");
const info = getIdCardInfo("11204416541220243x");

/**
{
  idCard: '11204416541220243x',
  addr: { province: '北京市', city: '', county: '' },
  addrCode: '112044',
  age: 367,
  gender: '男',
  birthday: '1654-12-20'
}
 */
```

也可以用命令行玩下

```bash
npm i get-id-card-info -g
npm uninstall get-id-card-info -g
get-id-card-info 11204416541220243x
```
