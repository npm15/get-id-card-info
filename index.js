const IdCard = require('./utils')
const getIdCardInfo =(idCard)=>{
  const IdCardInstance = new IdCard(idCard)
  return IdCardInstance.info
}
module.exports = getIdCardInfo