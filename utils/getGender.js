// https://www.jianshu.com/p/c8705a457d05
/**
 * 通过身份证判断性别
 * @param {string} idCard
 * @return {string} gender
 */

function getGender(idCard) {
  var genderStr = "";
  if (parseInt(idCard.slice(-2, -1)) % 2 == 1) {
    genderStr = "男";
  } else {
    genderStr = "女";
  }
  return genderStr;
}
module.exports = getGender;
