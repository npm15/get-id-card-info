const getAddr = require("./getAddr");
const getAddrCode = require("./getAddrCode");
const getAge = require("./getAge");
const getGender = require("./getGender");
const getBirthday = require("./getBirthday");
class IdCard {
  constructor(idCard) {
    this.idCard = idCard;
    this.info = this.getInfo();
  }
  getAddr = getAddr;
  getAddrCode = getAddrCode;
  getAge = getAge;
  getGender = getGender;
  getBirthday = getGender;
  getInfo() {
    return {
      idCard: this.idCard,
      addr: getAddr(this.idCard),
      addrCode: getAddrCode(this.idCard),
      age: getAge(this.idCard),
      gender: getGender(this.idCard),
      birthday: getBirthday(this.idCard),
    };
  }
}
module.exports = IdCard;
