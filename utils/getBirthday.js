// https://www.jianshu.com/p/c8705a457d05
/**
 * 通过身份证判断性别
 * @param {string} idCard
 * @return {string} birthday
 */
function getBirthday(idCard) {
  var birthday = "";
  if (idCard != null && idCard != "") {
    if (idCard.length == 15) {
      birthday = "19" + idCard.substr(6, 6);
    } else if (idCard.length == 18) {
      birthday = idCard.substr(6, 8);
    }

    birthday = birthday.replace(/(.{4})(.{2})/, "$1-$2-");
  }

  return birthday;
}
module.exports = getBirthday;
