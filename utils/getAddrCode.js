// https://www.jianshu.com/p/c8705a457d05
/**
 * 通过身份证判断性别
 * @param {string} idCard
 * @return {string} addrCode
 */

 function getAddrCode (idCard){
  return idCard.toString().slice(0,6);
};
module.exports = getAddrCode ;
