// https://www.jianshu.com/p/c8705a457d05
const addrCodeMap = require('./addrCodeMap')
/**
 * 通过身份证判断性别
 * @param {string} idCard
 * @return {object} addr
 */
function getAddr(idCard) {
  if (!idCard || idCard.length < 6) return "";
  var provinceNo = idCard.substring(0, 2) + "0000";
  var cityNo = idCard.substring(0, 4) + "00";
  var countyNo = idCard.substring(0, 6);
  var province = addrCodeMap.province[provinceNo] || "";
  var city = addrCodeMap.city[cityNo] || "";
  var county = addrCodeMap.county[countyNo] || "";
  return { province, city, county };
}
module.exports = getAddr ;
